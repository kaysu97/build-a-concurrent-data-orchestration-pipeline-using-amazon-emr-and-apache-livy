# Build a Concurrent Data Orchestration Pipeline Using Amazon EMR and Apache Livy
<img src="https://i.imgur.com/WKyz9VC.png" width="80%" height="60%" alt=""/>

## Scenario
In this lab, we will use Amazon EMR and Apache Spark to build scalable big data pipelines. Most of time, data scientists have to process complex data from a variety of sources. This data must be transformed to make it useful to downstream applications, such as machine learning pipelines, analytics dashboards, and business reports. Such pipelines often require Spark jobs to be run in parallel on Amazon EMR. 

This lab will focus on how to submit multiple Spark jobs in parallel on an EMR cluster using [Apache Livy](https://docs.aws.amazon.com/emr/latest/ReleaseGuide/emr-livy.html), which is available in EMR version 5.9.0 and later.

We use [Apache Airflow](https://airflow.apache.org/index.html) to orchestrate the data pipeline. Airflow is an open-sourced task scheduler that helps manage ETL tasks. With Airflow’s Configuration as Code approach, automating the generation of workflows, ETL tasks, and dependencies is easy. It helps customers shift their focus from building and debugging data pipelines to focusing on the business problems.

We use AWS CloudFormation in simple JSON or YAML template to launch the AWS services required to create this workflow.
In this case, the template includes the following:
* Amazon Elastic Compute Cloud (Amazon EC2) instance where the Airflow server is to be installed.
* Amazon Relational Database Service (Amazon RDS) instance, which stores the metadata for the Airflow server. Airflow interacts with its metadata using the SqlAlchemy library. Airflow recommends using MYSQL or Postgres. We use a PostgreSQL RDS instance.
* AWS Identity and Access Management (IAM) roles that allow the EC2 instance to interact with the RDS instance.
* Amazon Simple Storage Service (S3) bucket with the movielens data downloaded in it. The output of the transformed data is also be written into this bucket.

For demonstration purposes, we use the [movielens](https://grouplens.org/datasets/movielens/latest/) dataset to concurrently convert the csv files to parquet format and save it to Amazon S3. Each dataset file is a comma-separated file with a single header row. The following table describes each file in the dataset.

<img src="https://i.imgur.com/nORVZ8a.png" width="50%" height="50%" alt=""/>

## Prerequisites
* The workshop’s region should be in ‘N.Virginia’.
* Windows user can download Putty and PuTTYgen: 
    If you don’t have the PuTTy client/PuTTYgen installed on your machine, you can download and then launch it from here: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
* Remember tag your name when you create anything to make sure which one belonged to you. Or just remember the ID.
## Lab tutorial
###  Step 1: Create an Amazon EC2 key pair
* Remind again: Make sure to choose the region in "N.Virginia".
* If you already have a key pair, then you can skip to Step 2.
* To build this ETL pipeline, connect to an EC2 instance using SSH. 
    * Open the AWS Management Console and navigate to the EC2 console. In the EC2 console left navigation pane, choose Key Pairs.
        
        <img src="https://i.imgur.com/sFmOWYQ.png" width="30%" height="30%" alt=""/>
       
    * Create a key pair, type airflow_key_name (if your name is Ann,your key pair will be airflow_key_Ann), then choose Create. 
    * Be sure to keep the file you just downloaded in a safe and private place. Without access to this file, you lose the ability to use SSH to connect with your EC2 instance.
 
### Step 2: Execute the CloudFormation Script
1. The URL below is the S3 link where the CloudFormation script is. 
    
    https://s3.amazonaws.com/aws-bigdata-blog/artifacts/airflow.livy.emr/airflow.yaml
    * Fill up S3 URL with the link above. 
        ![](https://i.imgur.com/slyFNBG.png)
    * If URL is invalid , you can download "airflow.yaml" file in this lab. And choose "Upload a template file" instead of Amazon S3 URL in "Specify template" section.
2. On the next page, choose the key pair that you created in the previous step (airflow_key_name) along with a S3 bucket name. The S3 bucket should NOT exist as the cloudformation creates a new S3 bucket. 
    * Fill up your stack name. "airflow-name-stack" is recommended.
    * You can use the default DBPassword in the CloudFormation script or create by yourself.
    * Choose the key pair you just created(airflow_key_name).
    * Type S3 bucket name which has to be lower case. "movielens-name-emr" is recommended.
        ![](https://i.imgur.com/a5cxGYC.png)
3.  On the Next page, choose IAM role.
    * It's optional to fill up Tags which will help you better find your stack.
    * Choose IAM role with "CloudFormation-admin". It gives permission to CloudFormation to build up your template. Because your user credential doesn't have enough privilege to build it up.   
![](https://i.imgur.com/zGix7Cv.png)
4. Finally, review all the settings on the next page. 
    * Select the box marked "I acknowledge that AWS CloudFormation might create IAM resources" (this is required since the script creates IAM resources), then choose Create. 
         ![](https://i.imgur.com/2o5YxJI.png)
    * This creates all the resources required for this pipeline and takes some time to run. To view the stack’s progress, select the stack you created and choose the Events section or panel.
    * The stack isn't finished when the status shows "CREATE_IN_PROGRESS" until it turns to CREATE_COMPLETE. 
        ![](https://i.imgur.com/mn5RrVC.png)
### Step 3: Start the Airflow scheduler in the Airflow EC2 instance
1. You can use putty or CLI to access to EC2.
    * Note: See [Connecting to Your Linux Instance Using SSH.](https://gitlab.com/ecloudture/knowledge-base/aws-sop/aws-elastic-compute-cloud)
    ![](https://i.imgur.com/uV52qeI.png)
2. Now we need to run some commands as the root user.
```gherkin=
# sudo as the root user
sudo su
# Navigate to the airflow directory which was  created by the cloudformation template – Look     at the user-data section.
cd ~/airflow
source ~/.bash_profile
# Below is an image of how the ‘/root/airflow/’ directory should look like.
ls -ltr
```
![](https://i.imgur.com/dflRHaE.png)
3. Now we need to start the airflow scheduler. 
* The Airflow scheduler monitors all tasks and all directed acyclic graphs (DAGs), and triggers the task instances whose dependencies have been met. 
* In the background, it monitors and stays in sync with a folder for all DAG objects that it may contain. It periodically (every minute or so) inspects active tasks to see whether they can be triggered.
* The Airflow scheduler is designed to run as a persistent service in an Airflow production environment. To get it started, run the airflow scheduler. It will use the configuration specified in airflow.cfg.
* To start a scheduler, run the below command in your terminal.
```gherkin=
airflow scheduler
```
* Your screen should look like the following with scheduler running.

    <img src="https://i.imgur.com/ioQEAJo.png" width="80%" height="60%" alt=""/>
    
### Step 4: View the transform_movielens DAG on the Airflow Webserver
The Airflow webserver should be running on port 8080. 
1. To see the Airflow webserver, open any browser and type in the "EC2-public-dns-name":8080. The "EC2-public-dns-name" is the same one you just used to access to EC2 .
2. You should see a list of DAGs on the Airflow dashboard. We focus on the "transform_movielens" DAG for the purposes of this blog. Toggle the "ON" button next to the name of the DAG.
3. To run the DAG, go back to the Airflow dashboard, and choose the "Trigger DAG" button for the transform_movielens DAG.
![](https://i.imgur.com/c83SfJP.png)
4. Choose the transform_movielens DAG, then choose "Graph View"to view the following image.
    ![](https://i.imgur.com/39KYwf0.png)
    * This image shows the overall data pipeline. In the current setup, there are six transform tasks that convert each .csv file to parquet format from the movielens dataset. 
    * Parquet is a popular columnar storage data format used in big data applications. The DAG also takes care of spinning up and terminating the EMR cluster once the workflow is completed.
    * When the Airflow DAG is run, the first task calls the run_job_flow boto3 API to create an EMR cluster. The second task waits until the EMR cluster is ready to take on new tasks. 
    * As soon as the cluster is ready, the transform tasks are kicked off in parallel using Apache Livy, which runs on port 8998.
    * You can see concurrency in this Airflow DAG can run 3 tasks in parallel.
        ![](https://i.imgur.com/4reQVSA.png)
* The DAG code can also be viewed by choosing the Code button.    
* Concurrency in the current Airflow DAG is set to 3 (multiple spark sessions). Under the circumstances without overwhelming the EMR cluster, you can throttle the concurrency.
    ![](https://i.imgur.com/ZRLDM1g.png)
    
    ![](https://i.imgur.com/ONTemuA.png)
5. Let’s use one of the transform tasks as an example to understand the steps in detail.
* The first three lines of this code helps to look up the EMR cluster details. This is used to create an interactive spark session on the EMR cluster using Apache Livy.
```python=
# Converts each of the movielens datafile to parquet
def transform_movies_to_parquet(**kwargs):
    # ti is the Task Instance
    ti = kwargs['ti']
    cluster_id = ti.xcom_pull(task_ids='create_cluster')
    cluster_dns = emr.get_cluster_dns(cluster_id)
    headers = emr.create_spark_session(cluster_dns, 'spark')
    session_url = emr.wait_for_idle_session(cluster_dns, headers)
    statement_response = emr.submit_statement(session_url,   '/root/airflow/dags/transform/movies.scala')
    emr.track_statement_progress(cluster_dns, statement_response.headers)
    emr.kill_spark_session(session_url)
```
* create_spark_session
    Apache Livy creates an interactive spark session for each transform task. The code for which is shown below. SparkSession provides a single point of entry to interact with underlying Spark functionality and allows programming Spark with DataFrame and Dataset APIs. The Spark session is created by calling the POST /sessions API.
    * Note: You can also change different parameters like driverMemory, executor Memory, number of driver and executor cores as part of the API call.
```python=
# Creates an interactive scala spark session. 
# Python(kind=pyspark), R(kind=sparkr) and SQL(kind=sql) spark sessions can also be created by changing the value of kind.
def create_spark_session(master_dns, kind='spark'):
    # 8998 is the port on which the Livy server runs
    host = 'http://' + master_dns + ':8998'
    data = {'kind': kind}
    headers = {'Content-Type': 'application/json'}
    response = requests.post(host + '/sessions', data=json.dumps(data), headers=headers)
    logging.info(response.json())
    return response.headers
```
* submit_statement
    Once the session has completed starting up, it transitions to the idle state. The transform task is then submitted to the session. The scala code is submitted as a REST API call to the Livy Server instead of the EMR cluster, to have good fault tolerance and concurrency.
```python=
# Submits the scala code as a simple JSON command to the Livy server
def submit_statement(session_url, statement_path):
    statements_url = session_url + '/statements'
    with open(statement_path, 'r') as f:
        code = f.read()
    data = {'code': code}
    response = requests.post(statements_url, data=json.dumps(data), headers={'Content-Type': 'application/json'})
    logging.info(response.json())
    return response
```
6. Once the job is run successfully, the Spark session is ended and the EMR cluster is terminated.
## Conclusion
* **How does Apache Livy run the Scala code on the EMR cluster in parallel?**
Once the EMR cluster is ready, the transform tasks are triggered by the Airflow scheduler. Each transform task triggers Livy to create a new interactive spark session. Each POST request brings up a new Spark context with a Spark interpreter. This remote Spark interpreter is used to receive and run code snippets, and return back the result.
![](https://i.imgur.com/A2voVbl.png)

Congratulations! 😊 In this post, we explored:
* Orchestrate a Spark data pipeline on Amazon EMR using Apache Livy and Apache Airflow.
* Created a simple Airflow DAG to demonstrate how to run spark jobs concurrently. 
* You can modify concurrency to scale your ETL data pipelines and improve latency. 
* We saw how Livy helps to hide the complexity to submit spark jobs via REST by using optimal EMR resources.
* Additionally,the output data in S3 can be analyzed in Amazon Athena by creating a crawler on AWS Glue.
See [Building a Data Lake with AWS Glue and Amazon S3](https://github.com/ecloudvalley/Building-a-Data-Lake-with-AWS-Glue-and-Amazon-S3)
 
